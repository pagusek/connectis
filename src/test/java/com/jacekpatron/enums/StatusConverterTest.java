package com.jacekpatron.enums;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

/**
 * Test for class {@link StatusConverter}.
 * 
 * @author Jacek Patron
 *
 */
public final class StatusConverterTest
{

	private final StatusConverter underTest = new StatusConverter();

	@Test
	public void convertToDatabaseColumnTest()
	{
		// given
		final Status statusEnum = Status.ACCEPTED;

		// when
		final String statusDatabase = underTest.convertToDatabaseColumn(statusEnum);

		// then
		assertThat(statusDatabase).isEqualTo("ACCEPTED");
	}

	@Test
	public void convertToEntityAttributeTest()
	{
		// given
		final String statusDatabase = "ACCEPTED";

		// when
		final Status statusEnum = underTest.convertToEntityAttribute(statusDatabase);

		// then
		assertThat(statusEnum).isEqualTo(Status.ACCEPTED);
	}

}
