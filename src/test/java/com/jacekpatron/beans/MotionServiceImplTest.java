package com.jacekpatron.beans;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import com.jacekpatron.daos.MotionDao;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.exceptions.ConnectisException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public final class MotionServiceImplTest
{

	@Mock
	private MotionDao motionDao;

	@InjectMocks
	private final MotionServiceImpl underTest = new MotionServiceImpl();

	@Test
	public void createTest() throws ConnectisException
	{
		// given
		final MotionEntity motion = new MotionEntity();

		// when
		when(motionDao.create(anyString(), anyString())).thenReturn(motion);

		// then
		assertThat(underTest.create("Title", "Content")).isEqualTo(motion);
	}

}
