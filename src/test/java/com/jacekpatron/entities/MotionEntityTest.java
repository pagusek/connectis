package com.jacekpatron.entities;

import static org.fest.assertions.Assertions.assertThat;
import org.junit.Test;

/**
 * Test for class {@link MotionEntity}.
 * 
 * @author Jacek Patron
 *
 */
public final class MotionEntityTest
{

	private final MotionEntity underTest = new MotionEntity();

	@Test
	public void toStringTest()
	{
		// given
		underTest.setId(1L);
		underTest.setTitle("Jacek's report");

		// when
		final String underTestString = underTest.toString();

		// then
		assertThat(underTestString).isEqualTo("MotionEntity{Report id:=1, Report title:=Jacek's report}");
	}

}
