package com.jacekpatron.utils;

import static org.fest.assertions.Assertions.assertThat;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public class MotionUtilsTest
{
	@Test
	public void getLastValidMotionTest() throws ConnectisException
	{
		final MotionEntity motion1 = MotionEntity.builder().id(1L).title("1Z0-803").content("some content").status(Status.VERIFIED).build();
		final MotionEntity motion2 = MotionEntity.builder().id(2L).title("1Z0-803").content("some content").status(Status.REJECTED).build();
		final MotionEntity motion3 = MotionEntity.builder().id(3L).title("1Z0-803").content("some content").status(Status.CREATED).build();
		final MotionEntity motion4 = MotionEntity.builder().id(4L).title("1Z0-803").content("some content").status(Status.DELETED).build();
		final List<MotionEntity> motions = Arrays.asList(motion1, motion2, motion3, motion4);

		final MotionEntity lastValidMotion = MotionUtils.getLastValidMotion(motions);
		assertThat(lastValidMotion.getStatus()).isEqualTo(Status.REJECTED);
	}
}
