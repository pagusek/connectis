package com.jacekpatron.validators;

import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import org.junit.Test;

/**
 * Test for class {@link ConnectisValidator}.
 * 
 * @author Jacek Patron
 *
 */
public final class ConnectisValidatorTest
{
	@Test
	public void checkNotNullOrEmptyTest_positive() throws ConnectisException
	{
		ConnectisValidator.checkNotNullOrEmpty("report name", "title");
	}

	@Test(expected = ConnectisException.class)
	public void checkNotNullOrEmptyTest_negative() throws ConnectisException
	{
		ConnectisValidator.checkNotNullOrEmpty("", "title");
	}

	@Test
	public void checkStatus_positive() throws ConnectisException
	{
		ConnectisValidator.checkStatus(Status.CREATED, Status.DELETED);
		ConnectisValidator.checkStatus(Status.CREATED, Status.VERIFIED);

		ConnectisValidator.checkStatus(Status.VERIFIED, Status.REJECTED);
		ConnectisValidator.checkStatus(Status.VERIFIED, Status.ACCEPTED);

		ConnectisValidator.checkStatus(Status.ACCEPTED, Status.REJECTED);
		ConnectisValidator.checkStatus(Status.ACCEPTED, Status.PUBLISHED);
	}

	@Test(expected = ConnectisException.class)
	public void checkStatus_negative() throws ConnectisException
	{
		ConnectisValidator.checkStatus(Status.CREATED, Status.PUBLISHED);
	}
}
