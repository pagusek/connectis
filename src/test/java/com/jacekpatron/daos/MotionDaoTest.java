package com.jacekpatron.daos;

import static org.fest.assertions.Assertions.assertThat;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import com.jacekpatron.utils.MotionUtils;
import java.util.List;
import javax.ejb.EJB;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.testfun.jee.EjbWithMockitoRunner;

/**
 * Test for class {@link MotionDao}.
 * 
 * @author Jacek Patron
 *
 */
@RunWith(EjbWithMockitoRunner.class)
public final class MotionDaoTest
{

	@EJB
	private MotionDao motionDao;

	@Test
	public void createTest() throws ConnectisException
	{
		final MotionEntity savedMotion = motionDao.create("Title", "Content");

		final MotionEntity fetchedMotion = motionDao.findById(savedMotion.getId());

		assertThat(savedMotion).isEqualTo(fetchedMotion);
	}

	@Test(expected = ConnectisException.class)
	public void createTest_nullTitle() throws ConnectisException
	{
		motionDao.create(null, "Content");
	}

	@Test(expected = ConnectisException.class)
	public void createTest_blankContent() throws ConnectisException
	{
		motionDao.create("Title", "");
	}

	@Test
	public void deleteTest() throws ConnectisException
	{
		motionDao.create("1Z0-803", "Content");
		motionDao.delete("1Z0-803", "Reason");

		final List<MotionEntity> motionHistory = MotionUtils.getSortedMotions(motionDao.findByTitle("1Z0-803"));
		assertThat(motionHistory.get(0).getStatus()).isEqualTo(Status.CREATED);
		assertThat(motionHistory.get(1).getStatus()).isEqualTo(Status.DELETED);

	}
}
