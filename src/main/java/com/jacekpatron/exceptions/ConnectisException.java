package com.jacekpatron.exceptions;

import lombok.Getter;

@Getter
public class ConnectisException extends Exception
{
	private final int error;
	private final String errorDescription;
	private final String howToSolve;

	static String getDescription(final ConnectisError error, final String errorDescription, final String howToSolve)
	{
		return "Error: " + error.getCode() + ". " + errorDescription + ". How to solve: " + howToSolve + ".";
	}

	public ConnectisException(final ConnectisError error, final String errorDescription, final String howToSolve) {
		super(getDescription(error, errorDescription, howToSolve));
		this.error = error.getCode();
		this.errorDescription = errorDescription;
		this.howToSolve = howToSolve;
	}
}
