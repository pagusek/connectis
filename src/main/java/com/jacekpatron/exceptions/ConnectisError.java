package com.jacekpatron.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ConnectisError {

	//@formatter:off
	PARAMETER_VALUE_MISSING(100), 
	WRONG_STATUS(101), 
	PARAMETER_NOT_UNIQUE(102),
	BROKEN_WORKFLOW(103);
	//@formatter:on

	private final int code;
}
