package com.jacekpatron.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatusConverter implements AttributeConverter<Status, String>
{

	@Override
	public String convertToDatabaseColumn(Status enumStatus)
	{
		return enumStatus.name();
	}

	@Override
	public Status convertToEntityAttribute(String databaseStatus)
	{
		return Status.valueOf(databaseStatus);
	}

}
