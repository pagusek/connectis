package com.jacekpatron.enums;

import java.util.Arrays;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;

public enum Status {

	//@formatter:off
	DELETED,
	REJECTED,
	PUBLISHED,
	ACCEPTED,
	VERIFIED,
	CREATED;
	//@formatter:on
	
	@Getter
	private List<Status> possibleOptions = Lists.newArrayList();
	
	static {
		CREATED.possibleOptions.addAll(Arrays.asList(Status.DELETED, Status.VERIFIED));
		VERIFIED.possibleOptions.addAll(Arrays.asList(Status.REJECTED, Status.ACCEPTED));
		ACCEPTED.possibleOptions.addAll(Arrays.asList(Status.REJECTED, Status.PUBLISHED));
	}
	
}
