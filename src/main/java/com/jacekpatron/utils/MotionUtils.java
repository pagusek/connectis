package com.jacekpatron.utils;

import static com.jacekpatron.validators.ConnectisValidator.checkWorkflow;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.Arrays;
import java.util.List;

public class MotionUtils
{
	public static MotionEntity getLastValidMotion(final List<MotionEntity> motions) throws ConnectisException
	{
		checkWorkflow(motions);
		return Iterables.getLast(getSortedMotions(motions));
	}

	public static List<MotionEntity> getSortedMotions(final List<MotionEntity> motions)
	{
		return byStatusOrder.onResultOf(byStatusFunction).sortedCopy(motions);
	}

	//@formatter:off
	private static Ordering<Status> byStatusOrder = Ordering.explicit(Arrays.asList(
		Status.CREATED, 
		Status.DELETED, 
		Status.VERIFIED, 
		Status.ACCEPTED, 
		Status.REJECTED, 
		Status.PUBLISHED
	));
	//@formatter:on

	private static Function<MotionEntity, Status> byStatusFunction = new Function<MotionEntity, Status>() {
		@Override
		public Status apply(MotionEntity motion)
		{
			return motion.getStatus();
		}
	};
}
