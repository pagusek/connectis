package com.jacekpatron.validators;

import com.google.common.base.Strings;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisError;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.List;

public class ConnectisValidator
{

	public static final void checkNotNullOrEmpty(final String value, final String validatedField) throws ConnectisException
	{
		if (Strings.isNullOrEmpty(value)) {
			throw new ConnectisException(ConnectisError.PARAMETER_VALUE_MISSING, validatedField + " value cannot be missing",
				"please provide mandatory fields");
		}
	}

	public static final void checkWorkflow(final List<MotionEntity> motions) throws ConnectisException
	{
		if (motions == null || motions.isEmpty()) {
			throw new ConnectisException(ConnectisError.BROKEN_WORKFLOW, "workflow is not in consistency state", "please check history of motion in database");
		}
	}

	public static final void checkStatus(final Status currentStatus, final Status nextStatus) throws ConnectisException
	{
		boolean isChangePossible = true;

		switch (currentStatus) {
		case CREATED:
		case VERIFIED:
		case ACCEPTED:
			isChangePossible = currentStatus.getPossibleOptions().contains(nextStatus);
			break;
		default:
			isChangePossible = false;
		}

		if (!isChangePossible) {
			throw new ConnectisException(ConnectisError.WRONG_STATUS,
				"you cannot move motion from " + currentStatus.name() + " to " + nextStatus.name() + " status", "please change the status according to spec");
		}
	}
}
