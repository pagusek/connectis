package com.jacekpatron.dtos;

import com.jacekpatron.exceptions.ConnectisException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import lombok.Getter;

@Getter
public class ConnectisErrorDto
{
	private static final Status ERR_STATUS = Status.BAD_REQUEST;

	private final String httpError;
	private final int connectisError;
	private final String errorDescription;
	private final String howToSolve;

	public ConnectisErrorDto(final ConnectisException ce) {
		httpError = "HTTP " + ERR_STATUS.getStatusCode() + " " + ERR_STATUS.getReasonPhrase();
		this.connectisError = ce.getError();
		this.errorDescription = ce.getErrorDescription();
		this.howToSolve = ce.getHowToSolve();
	}

	public Response response()
	{
		return Response.status(ERR_STATUS).entity(this).build();
	}
}
