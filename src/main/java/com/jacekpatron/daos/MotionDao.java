package com.jacekpatron.daos;

import static com.jacekpatron.validators.ConnectisValidator.checkNotNullOrEmpty;
import static com.jacekpatron.validators.ConnectisValidator.checkStatus;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import com.jacekpatron.utils.MotionUtils;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.apache.commons.lang.StringUtils;

@Stateless
public class MotionDao
{
	@PersistenceContext(name = "connectis_pu")
	EntityManager em;

	public MotionEntity create(final String title, final String content) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		checkNotNullOrEmpty(content, "content");

		final MotionEntity motion = new MotionEntity();
		motion.setTitle(title);
		motion.setContent(content);
		motion.setStatus(Status.CREATED);

		em.persist(motion);
		em.flush();

		return motion;
	}

	public MotionEntity delete(final String title, final String reason) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		checkNotNullOrEmpty(reason, "reason");
		final MotionEntity motionFromPreviousStage = MotionUtils.getLastValidMotion(findByTitle(title));
		checkStatus(motionFromPreviousStage.getStatus(), Status.DELETED);

		final MotionEntity motionInStatusDeleted = new MotionEntity();
		setFieldsFromPreviousMotion(motionInStatusDeleted, motionFromPreviousStage);
		motionInStatusDeleted.setStatus(Status.DELETED);
		motionInStatusDeleted.setReason(reason);

		em.persist(motionInStatusDeleted);
		em.flush();

		return motionInStatusDeleted;
	}

	public MotionEntity verify(final String title, final String content) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		final MotionEntity motionFromPreviousStage = MotionUtils.getLastValidMotion(findByTitle(title));
		checkStatus(motionFromPreviousStage.getStatus(), Status.VERIFIED);

		final MotionEntity motionInStatusVerified = new MotionEntity();
		motionInStatusVerified.setTitle(title);
		if (StringUtils.isNotBlank(content)) {
			motionInStatusVerified.setContent(content);
		} else {
			motionInStatusVerified.setContent(motionFromPreviousStage.getContent());
		}
		motionInStatusVerified.setStatus(Status.VERIFIED);

		em.persist(motionInStatusVerified);
		em.flush();

		return motionInStatusVerified;
	}

	public MotionEntity reject(final String title, final String reason) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		checkNotNullOrEmpty(reason, "reason");
		final MotionEntity motionFromPreviousStage = MotionUtils.getLastValidMotion(findByTitle(title));
		checkStatus(motionFromPreviousStage.getStatus(), Status.REJECTED);

		final MotionEntity motionInStatusRejected = new MotionEntity();
		setFieldsFromPreviousMotion(motionInStatusRejected, motionFromPreviousStage);
		motionInStatusRejected.setStatus(Status.REJECTED);
		motionInStatusRejected.setReason(reason);

		em.persist(motionInStatusRejected);
		em.flush();

		return motionInStatusRejected;
	}

	public MotionEntity accept(final String title) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		final MotionEntity motionFromPreviousStage = MotionUtils.getLastValidMotion(findByTitle(title));
		checkStatus(motionFromPreviousStage.getStatus(), Status.ACCEPTED);

		final MotionEntity motionInStatusAccepted = new MotionEntity();
		setFieldsFromPreviousMotion(motionInStatusAccepted, motionFromPreviousStage);
		motionInStatusAccepted.setStatus(Status.ACCEPTED);

		em.persist(motionInStatusAccepted);
		em.flush();

		return motionInStatusAccepted;
	}

	public MotionEntity publish(final String title) throws ConnectisException
	{
		checkNotNullOrEmpty(title, "title");
		final MotionEntity motionFromPreviousStage = MotionUtils.getLastValidMotion(findByTitle(title));
		checkStatus(motionFromPreviousStage.getStatus(), Status.PUBLISHED);

		final MotionEntity motionInStatusPublished = new MotionEntity();
		setFieldsFromPreviousMotion(motionInStatusPublished, motionFromPreviousStage);
		motionInStatusPublished.setStatus(Status.PUBLISHED);
		// millis will provide always unique number
		motionInStatusPublished.setMotionNo(new Date().getTime());

		em.persist(motionInStatusPublished);
		em.flush();

		return motionInStatusPublished;
	}

	public MotionEntity findById(final Long id)
	{
		return em.find(MotionEntity.class, id);
	}

	public List<MotionEntity> findByTitle(final String title)
	{
		final TypedQuery<MotionEntity> query = em.createQuery("SELECT m FROM MotionEntity m WHERE m.title = :title", MotionEntity.class);
		query.setParameter("title", title);
		return query.getResultList();
	}

	public List<MotionEntity> findMotions(Integer pagination, String title, Status status)
	{
		final TypedQuery<MotionEntity> query = em.createQuery("SELECT m FROM MotionEntity m WHERE m.title = :title OR m.status = :status", MotionEntity.class);
		query.setParameter("title", title);
		query.setParameter("status", status);
		query.setMaxResults(pagination);
		return query.getResultList();
	}

	private void setFieldsFromPreviousMotion(final MotionEntity newMotion, final MotionEntity previousMotion)
	{
		newMotion.setTitle(previousMotion.getTitle());
		newMotion.setContent(previousMotion.getContent());
	}
}
