package com.jacekpatron.beans;

import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.List;
import javax.ejb.Remote;

@Remote
public interface MotionService
{
	public MotionEntity create(final String title, final String content) throws ConnectisException;

	public MotionEntity delete(final String title, final String reason) throws ConnectisException;

	public MotionEntity verify(final String title, final String content) throws ConnectisException;

	public MotionEntity reject(final String title, final String reason) throws ConnectisException;

	public MotionEntity accept(final String title) throws ConnectisException;

	public MotionEntity publish(final String title) throws ConnectisException;

	public List<MotionEntity> getMotions(final Integer pagination, final String title, final Status status);

}
