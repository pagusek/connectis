package com.jacekpatron.beans;

import com.jacekpatron.daos.MotionDao;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.enums.Status;
import com.jacekpatron.exceptions.ConnectisError;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class MotionServiceImpl implements MotionService
{

	@Inject
	private MotionDao motionDao;

	@Override
	public MotionEntity create(final String title, final String content) throws ConnectisException
	{
		isTitleUnique(title);
		return motionDao.create(title, content);
	}

	@Override
	public MotionEntity delete(final String title, final String reason) throws ConnectisException
	{
		return motionDao.delete(title, reason);
	}

	@Override
	public MotionEntity verify(final String title, final String content) throws ConnectisException
	{
		return motionDao.verify(title, content);
	}

	@Override
	public MotionEntity reject(final String title, final String reason) throws ConnectisException
	{
		return motionDao.reject(title, reason);
	}

	@Override
	public MotionEntity accept(final String title) throws ConnectisException
	{
		return motionDao.accept(title);
	}

	@Override
	public MotionEntity publish(final String title) throws ConnectisException
	{
		return motionDao.publish(title);
	}

	private void isTitleUnique(String title) throws ConnectisException
	{
		final int count = motionDao.findByTitle(title).size();
		if (count > 0) {
			throw new ConnectisException(ConnectisError.PARAMETER_NOT_UNIQUE, title + " is not unique", "please provide unique motion title");
		}
	}

	@Override
	public List<MotionEntity> getMotions(Integer pagination, String title, Status status)
	{
		return motionDao.findMotions(pagination, title, status);
	}

}
