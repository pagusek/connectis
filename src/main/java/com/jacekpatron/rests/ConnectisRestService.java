package com.jacekpatron.rests;

import com.jacekpatron.beans.MotionService;
import com.jacekpatron.dtos.ConnectisErrorDto;
import com.jacekpatron.entities.MotionEntity;
import com.jacekpatron.exceptions.ConnectisException;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("api/v1/motions")
public class ConnectisRestService extends Application
{

	@EJB
	private MotionService motionService;

	@POST
	@Produces("application/json")
	@Path("create")
	public Response create(@FormParam("title") String title, @FormParam("content") String content)
	{
		try {
			motionService.create(title, content);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@POST
	@Produces("application/json")
	@Path("delete")
	public Response delete(@FormParam("title") String title, @FormParam("reason") String reason)
	{
		try {
			motionService.delete(title, reason);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@POST
	@Produces("application/json")
	@Path("verify")
	public Response verify(@FormParam("title") String title, @FormParam("content") String content)
	{
		try {
			motionService.verify(title, content);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@POST
	@Produces("application/json")
	@Path("reject")
	public Response reject(@FormParam("title") String title, @FormParam("reason") String reason)
	{
		try {
			motionService.reject(title, reason);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@POST
	@Produces("application/json")
	@Path("accept")
	public Response accept(@FormParam("title") String title)
	{
		try {
			motionService.accept(title);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@POST
	@Produces("application/json")
	@Path("publish")
	public Response publish(@FormParam("title") String title)
	{
		try {
			motionService.publish(title);
			return Response.status(Status.OK).entity("HTTP 201 Created").build();
		} catch (ConnectisException ce) {
			return new ConnectisErrorDto(ce).response();
		}
	}

	@GET
	@Produces("application/json")
	@Path("browse/{pagination}/{title}/{status}")
	public Response browse(@DefaultValue("10") @PathParam("pagination") Integer pagination, @PathParam("title") String title, @PathParam("status") com.jacekpatron.enums.Status status)
	{
		List<MotionEntity> motions = motionService.getMotions(pagination, title, status);
		return Response.status(Status.OK).entity(motions).build();
	}
}
