package com.jacekpatron.entities;

import com.google.common.base.MoreObjects;
import com.jacekpatron.enums.Status;
import com.jacekpatron.enums.StatusConverter;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity representation of <b>MOTION</b> database table.
 * 
 * @author Jacek Patron
 *
 */
@Entity
@Table(name = "MOTION")
@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
public class MotionEntity implements Serializable
{

	private static final long serialVersionUID = -2317131776621383703L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	private String title;

	@NotNull
	private String content;

	@Convert(converter = StatusConverter.class)
	private Status status;

	@Column(nullable = true)
	private String reason;

	@Column(nullable = true)
	private long motionNo;

	@Override
	public String toString()
	{
		//@formatter:off
		return MoreObjects.toStringHelper(MotionEntity.class).
			add("Report id:", id).
			add("Report title:", title).
		toString();
		//@formatter:on
	}
}
